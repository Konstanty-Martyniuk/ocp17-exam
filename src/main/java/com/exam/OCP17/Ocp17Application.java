package com.exam.OCP17;

import chapter6.Animal3;
import chapter7.Crane;
import chapter7.Crane2Overloaded;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ocp17Application {

	public static void main(String[] args) {
		var zebra = new Animal3();

		var mommy = new Crane(4, "Cammy");
		var overloadedMommy = new Crane2Overloaded("Super", "Muddy");
		System.out.println(overloadedMommy);
	}
}
