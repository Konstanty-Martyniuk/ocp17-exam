package chapter8;

public interface LearnToSpeak {
    void speak(String sound);
}
