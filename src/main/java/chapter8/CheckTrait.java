package chapter8;

public interface CheckTrait {
    boolean test(Animal a);
}
