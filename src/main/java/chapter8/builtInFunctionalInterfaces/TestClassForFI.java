package chapter8.builtInFunctionalInterfaces;

import java.time.LocalDate;
import java.util.function.Predicate;
import java.util.function.Consumer;
import java.util.function.BooleanSupplier;
import java.util.HashMap;

public class TestClassForFI {
    public static void main(String[] args) {

        /////////////////////SUPPLIER

        Supplier<LocalDate> s1 = LocalDate::now;
        Supplier<LocalDate> s2 = () -> LocalDate.now();

        LocalDate d1 = s1.get();
        LocalDate d2 = s2.get();
        System.out.println(d1);
        System.out.println(d2);

        /////////////////////CONSUMER

        Consumer<String> c1 = System.out::println;
        Consumer<String> c2 = c -> System.out.println(c);
        c1.accept("Kotiara");

        var map = new HashMap<String, Integer>();
        BiConsumer<String, Integer> b1 = map::put;
        BiConsumer<String, Integer> b2 = (x, y) -> map.put(x, y);

        b1.accept("chicken", 7);
        b2.accept("chick", 1);
        System.out.println(map);

        ///////////////////////////PREDIСATE
        Predicate<String> p1 = String::isEmpty;
        Predicate<String> p2 = s -> s.isEmpty();

        System.out.println(p1.test(""));
        System.out.println(p2.test(""));

        BiPredicate<String, String> bp1 = String::startsWith;
        BiPredicate<String, String> bp2 = (x, y) -> x.startsWith(y);

        System.out.println(bp1.test("chicken", "chick")); // true
        System.out.println(bp2.test("chicken", "chick")); // true

        ///////////////////////////FUNCTION
        Function<String, Integer> f1 = String::length;
        Function<String, Integer> f2 = s -> s.length();

        System.out.println(f1.apply("cluck"));
        System.out.println(f2.apply("cluck"));

        BiFunction<String, String, String> bf1 = String::concat;
        BiFunction<String, String, String> bf2 = (x, y) -> x.concat(y);

        ///////////////////////////UNARYOPERATOR
        UnaryOperator<String> u1 = String::toUpperCase;
        UnaryOperator<String> u2 = x -> x.toUpperCase();

        System.out.println(u1.apply("chirp")); // CHIRP
        System.out.println(u2.apply("chirp")); // CHIRP

        BinaryOperator<String> bo1 = String::concat;
        BinaryOperator<String> bo2 = (string, toAdd) -> string.concat(toAdd);
        System.out.println(bo1.apply("baby ", "chick")); // baby chick
        System.out.println(bo2.apply("baby ", "chick")); // baby chick

        Predicate<String> egg = s -> s.contains("egg");
        Predicate<String> brown = s -> s.contains("brown");

        Predicate<String> brownEggs = s -> s.contains("egg") && s.contains("brown");
        Predicate<String> otherEggs = s -> s.contains("egg") && !s.contains("brown");
        Predicate<String> brownEggs2 = egg.and(brown);

        Consumer<String> cd1 = x -> System.out.print("1: " + x);
        Consumer<String> cd2 = x -> System.out.print(",2: " + x);
        Consumer<String> combined = cd1.andThen(cd2);
        combined.accept("Annie"); // 1: Annie,2: Annie

        BooleanSupplier bs1 = () -> true;
        BooleanSupplier bs2 = () -> Math.random() > .5;
        System.out.println(bs1.getAsBoolean()); // true
        System.out.println(bs2.getAsBoolean()); // false

    }
}
