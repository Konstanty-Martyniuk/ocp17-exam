package chapter8.builtInFunctionalInterfaces;
@FunctionalInterface
public interface Function<T, R> {
    R apply(T t);
}
