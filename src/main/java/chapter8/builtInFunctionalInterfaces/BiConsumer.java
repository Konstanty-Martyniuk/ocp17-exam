package chapter8.builtInFunctionalInterfaces;
@FunctionalInterface
public interface BiConsumer<T, U> {
    void accept(T t, U u);
}
