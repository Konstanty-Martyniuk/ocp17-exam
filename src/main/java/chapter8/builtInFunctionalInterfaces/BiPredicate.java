package chapter8.builtInFunctionalInterfaces;
@FunctionalInterface
public interface BiPredicate<T, U> {
    boolean test(T t, U u);
}
