package chapter8.builtInFunctionalInterfaces;
@FunctionalInterface
public interface Consumer<T> {
    void accept(T t);
}
