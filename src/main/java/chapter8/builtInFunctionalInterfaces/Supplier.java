package chapter8.builtInFunctionalInterfaces;
@FunctionalInterface
public interface Supplier<T> {
    T get();
}
