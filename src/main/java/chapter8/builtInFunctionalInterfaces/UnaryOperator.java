package chapter8.builtInFunctionalInterfaces;
@FunctionalInterface
public interface UnaryOperator<T> extends Function<T, T> {
    T apply(T t);
}
