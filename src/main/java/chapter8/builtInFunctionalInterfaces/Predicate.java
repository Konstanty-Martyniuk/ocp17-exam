package chapter8.builtInFunctionalInterfaces;
@FunctionalInterface
public interface Predicate<T> {
    boolean test(T t);
}
