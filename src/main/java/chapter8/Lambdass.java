package chapter8;

import java.util.*;

public class Lambdass {
    public static void main(String[] args) {
        // list of animals
        var animals = new ArrayList<Animal>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        // pass class that does check
        print(animals, new CheckIfHopper());
        print(animals, a ->a.canSwim());
        print(animals, Animal::canSwim);


        Converter methodRef = Math::round;
        Converter lambda = x -> Math.round(x);
        System.out.println(methodRef.round(100.1)); // 100

        var str = "Zoo";
        StringStart methodRef2 = str::startsWith;
        StringStart lambda2 = s -> s.startsWith(s);
        System.out.println(methodRef2.beginningCheck("A")); // false

        var str2 = "";
        StringChecker methodRef3 = str2::isEmpty;
        StringChecker lambda3 = () -> str2.isEmpty();

        StringParameterChecker methodRef4 = String::isEmpty;
        StringParameterChecker lambda4 = s -> str.isEmpty();
        System.out.println(methodRef4.check("Zoo")); // false


        StringTwoParameterChecker methodRef5 = String::startsWith;
        StringTwoParameterChecker lambda5 = (s,p) -> s.startsWith(p);

        System.out.println(methodRef5.check("Zoo", "A")); // false

        EmptyStringCreator methodRef6 = String::new;
        EmptyStringCreator lambda6 = () -> new String();
        var myString = methodRef6.create();

        StringCopier methodRef7 = String::new;
        StringCopier lambda7 = s -> new String(s);

    }

    private static void print(List<Animal> animals, CheckTrait checker) {
        for (Animal animal : animals) {
            // General check
            if (checker.test(animal))
                System.out.print(animal + " ");
        }
        System.out.println();
    }
}
