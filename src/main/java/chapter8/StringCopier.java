package chapter8;

public interface StringCopier {
    String copy(String text);
}
