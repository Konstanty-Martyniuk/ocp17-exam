package chapter8;

public interface StringChecker {
    boolean check();
}
