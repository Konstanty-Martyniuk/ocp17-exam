package chapter8;

public interface EmptyStringCreator {
    String create();
}
