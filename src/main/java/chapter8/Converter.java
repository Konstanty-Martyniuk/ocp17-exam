package chapter8;

public interface Converter {
    long round(double num);
}
