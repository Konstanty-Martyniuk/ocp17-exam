package chapter8;

public class DuckHelper {
    public static void teacher(String name, LearnToSpeak trainer) {
        trainer.speak(name);
    }
}
