package chapter8;
@FunctionalInterface
public interface Sprint {
    public void sprint(int speed);
}
