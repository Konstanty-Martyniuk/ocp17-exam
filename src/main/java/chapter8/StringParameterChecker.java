package chapter8;

public interface StringParameterChecker {
    boolean check(String text);
}
