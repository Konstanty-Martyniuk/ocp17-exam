package chapter5;

public class LaFinal {
    final int age = 10;
    final int fishEaten;
    final String name;

    {
        fishEaten = 10; //starts before everything
    }

    public LaFinal() {
        name = "Robert";
    }
}
