package chapter5;

/**
 * Rules for Creating a Method with a Varargs Parameter
 * 1. A method can have at most one varargs parameter.
 * 2. If a method contains a varargs parameter, it must be the last parameter in the list.
 */
public class VarArgs {
    public void walk1(int... steps) {}
    public void walk2(int start, int... steps) {}

    public static void run(int... steps) {
        System.out.print(steps[1]);
        }
    public static void main(String[] args) {
        run(11, 77); // 77

        walkDog(1); // 0
        walkDog(1, 2); // 1
        walkDog(1, 2, 3); // 2
        walkDog(1, new int[] {4, 5}); // 2
    }

    public static void walkDog(int start, int... steps) {
        System.out.println(steps.length);
    }
}
