package chapter7;

public enum SeasonWithAbstractMethod {

    SPRING {
        public String getHours() {
            return "9am-7pm";
        }
    },
    SUMMER {
        public String getHours() {
            return "9am-7pm";
        }
    },
    WINTER, AUTUMN;
    public String getHours() {
        return "9am-3pm";
    }

}
