package chapter7;

public interface Weather {
    int getAverageTemperature();
}
