package chapter7;

public class Fox {
    private class Den {}
    public void goHome() {
        new Den();
    }
    public static void visitFriend(Fox fox) {//pass fox
        //new Den(); // DOES NOT COMPILE
        fox.new Den();
    }
}
//public class Squirrel {
//    public void visitFox() {
//        //new Den(); // DOES NOT COMPILE - outside Fox, Dan is private
//    }
//}
