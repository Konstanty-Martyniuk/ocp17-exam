package chapter7;

import java.util.Locale;

public record Crane(int numberEggs, String name) {
    //long constructor
//    public Crane(int numberEggs, String name) {
//        if (numberEggs < 0) throw new IllegalArgumentException();
//        this.numberEggs = numberEggs;
//        this.name = name;
//    }

    //short constructor
    public Crane {
        if (numberEggs <= 0) throw new IllegalArgumentException();
        name = name.toUpperCase(Locale.ROOT);
    }
}
