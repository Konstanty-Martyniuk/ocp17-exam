package chapter7;

public record Crane2Overloaded(int numberEggs, String name) {
    public Crane2Overloaded(String firstName, String lastName) {
        this(2, firstName + " " + lastName);
    }
}
