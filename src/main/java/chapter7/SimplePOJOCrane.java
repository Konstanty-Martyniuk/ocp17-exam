package chapter7;

public class SimplePOJOCrane {
    private final int numberEggs;
    private final String name;

    public SimplePOJOCrane(int numberEggs, String name) {
        if (numberEggs > 0) {
            this.numberEggs = numberEggs;
        } else {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public int getNumberEggs() {
        return numberEggs;
    }

    public String getName() {
        return name;
    }
}
