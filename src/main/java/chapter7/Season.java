package chapter7;

public enum Season implements Weather {
    WINTER("Low"), SPRING("Medium"), SUMMER("High"), AUTUMN("Medium");
    private final String expectedVisitors;
    private Season(String expectedVisitors) {
        this.expectedVisitors = expectedVisitors;
    }

    public void printExpectedVisitors() {
        System.out.println(expectedVisitors);
    }

    @Override
    public int getAverageTemperature() {
        return 30;
    }
}
