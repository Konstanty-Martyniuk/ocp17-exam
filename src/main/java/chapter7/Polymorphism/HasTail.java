package chapter7.Polymorphism;

public interface HasTail {
    public abstract boolean isTailStriped();
}
