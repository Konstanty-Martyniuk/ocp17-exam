package chapter7;

public interface InterfaceBasics {
//      Interfaces are implicitly abstract.
//      Interface variables are implicitly public, static, and final.
//      Interface methods without a body are implicitly abstract.
//      Interface methods without the private modifier are implicitly public.
}
