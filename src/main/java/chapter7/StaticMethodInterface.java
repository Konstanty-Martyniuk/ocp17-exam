package chapter7;

public interface StaticMethodInterface {
//    1. A static method must be marked with the static keyword and include a method body.
//    2. A static method without an access modifier is implicitly public.
//    3. A static method cannot be marked abstract or final.
//    4. A static method is not inherited and cannot be accessed in a class implementing the
//    interface without a reference to the interface name.
}
