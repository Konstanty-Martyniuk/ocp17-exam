package chapter7;

public class JustClass {
    public static void main(String[] args) {
        var s = Season.SUMMER;
        System.out.println(s.toString().toLowerCase());

        Season t = Season.valueOf("SPRING");
        System.out.println(t);


        for (var season: Season.values()) {
            System.out.println(season.name() + " " + season.ordinal());
        }

        Season summer = Season.SUMMER;
        switch (summer) {
            case WINTER -> System.out.println("Time to play snowballs!");
            case SUMMER -> System.out.println("Time to go to the sea!");
            case AUTUMN -> System.out.println("Say goodbye to warm days");
            default -> System.out.println("Is it summer yet?");
        }

        summer.printExpectedVisitors();
        Season.SPRING.printExpectedVisitors();
    }
}
