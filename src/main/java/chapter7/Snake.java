package chapter7;

public class Snake implements IsColdBlooded {
    @Override
    public boolean hasScales() {
        return true;
    }

    @Override
    public double getTemperature() {
        return IsColdBlooded.super.getTemperature();
    }
}
