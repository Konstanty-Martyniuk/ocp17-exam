package chapter6;

public class Hamster {
    private String color;
    private int weight;

    public Hamster(int weight, String color) { // First constructor
        this.weight = weight;
        this.color = color;
    }

    public Hamster(int weight) { // Second constructor
//
//        this.weight = weight;
//        color = "brown";
        //better option
        this(weight, "brown"); //always first!
    }

    public static void main(String[] args) {
        Hamster hamster = new Hamster(3, "red");
    }
}
