package chapter6;

public class Jaguar extends Mammal {
    public Jaguar() {
        size = 10.2;
    }

    public void printDetails() {
        System.out.println(size);
    }
}
