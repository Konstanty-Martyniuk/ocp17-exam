package chapter6;

public class Duck {
    private String color;
    private int height;
    private int length;

    public Duck(int height, int length) {
        this.height = height;
        this.length = length;
        this.color = "white";
    }
}
