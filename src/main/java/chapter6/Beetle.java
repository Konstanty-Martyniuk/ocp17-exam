package chapter6;

public class Beetle extends Insect {
    protected int numberOflegs = 6;
    short age = 3;

    public void printData() {
        System.out.println(this.label);
        System.out.println(super.label);
        System.out.println(this.age);
        //System.out.println(super.age);
        System.out.println(numberOfLegs);
    }
}
