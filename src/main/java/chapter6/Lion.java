package chapter6;

public class Lion extends Animal {
    public Lion(int age) {
        super(age);
    }

    protected void setProperties(String name) {
        this.name = name;
    }

    public void roar() {
        System.out.print(name + ", age " + getAge() + ", says: Roar!");
    }

    public static void main(String[] args) {
        var lion = new Lion(3);
        lion.setProperties("kion");
        lion.roar();


        var lion2 = new Lion(2);
        lion2.setProperties("boobbyy");
        lion2.roar();
    }
}
