package chapter6;

import java.util.ArrayList;

public final class Animal3 {
    private final ArrayList<String> favoriteFoods;

    public Animal3() {
        this.favoriteFoods = new ArrayList<>();
        this.favoriteFoods.add("Apples");
    }

    public String getFavoriteFoodsItem(int index) {
        return favoriteFoods.get(index);
    }

    public int getFavoriteFoodsCount() {
        return favoriteFoods.size();
    }

    public ArrayList<String> getFavoriteFoods() {
        return new ArrayList<>(this.favoriteFoods);
    }
}
