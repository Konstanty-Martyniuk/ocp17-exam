package chapter3;

public class SwitchThemAll {
    public static void main(String[] args) {

        int x = 0;
        for(long y = 0, z = 4; x < 5 && y < 10; x++, y++) {
            System.out.print(y + " "); }
        System.out.print(x + " ");

//        int i;
//        for(i=0; i < 10; i++)
//            System.out.println("Value is: "+i);
//        System.out.println(i);
//
//
//        int fish = 5;
//        int length = 12;
//        var name = switch (fish) {
//            case 1 -> "Goldfish";
//            case 2 -> {
//                yield "Trout";
//            }
//            case 3 -> {
//                if (length > 10) yield "Blobfish";
//                else yield "Green";
//            }
//            default -> "Swordfish";
//        };

/*        int canis = 5;

        String type = switch (canis) { // DOES NOT COMPILE
            case 1 -> "dog";
            case 2 -> "wolf";
            case 3 -> "coyote";
        };*/
    }

    public void printSeason(int month) {
        switch (month) {
            case 1, 2, 3 -> System.out.print("Winter");
            case 4, 5, 6 -> System.out.print("Spring");
            case 7, 8, 9 -> System.out.print("Summer");
            case 10, 11, 12 -> System.out.print("Fall");
        }
    }
}
