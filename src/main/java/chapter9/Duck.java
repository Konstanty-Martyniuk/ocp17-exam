package chapter9;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Duck implements Comparable<Duck> {
    private String name;
    private int weight;
    @Override
    public int compareTo(Duck d) {
        return name.compareTo(d.name);
    }

    @Override
    public String toString() {
        return name;
    }
}
