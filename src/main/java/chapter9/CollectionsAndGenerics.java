package chapter9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CollectionsAndGenerics {
    public static void main(String[] args) {

        var ducks = new ArrayList<Duck>();
        ducks.add(new Duck("Quack", 7));
        ducks.add(new Duck("Puddles", 10));
        Collections.sort(ducks); // sort by name
        System.out.println(ducks); // [Puddles, Quack]

        Comparator<Duck> byWeight = new Comparator<Duck>() {
            @Override
            public int compare(Duck d1, Duck d2) {
                return Integer.compare(d1.getWeight(), d2.getWeight());
            }
        };
        Comparator<Duck> byWeight2 = Comparator.comparing(Duck::getWeight);

        Collections.sort(ducks);
        ducks.forEach(System.out::println);
        ducks.sort(byWeight);
        ducks.forEach(System.out::println);
    }
}
