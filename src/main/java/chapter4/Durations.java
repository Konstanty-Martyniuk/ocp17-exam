package chapter4;

import java.time.*;
import java.time.temporal.ChronoUnit;

/**
 * Duration is for the short - time
 */
public class Durations {
    public static void main(String[] args) {
        var daily = Duration.ofDays(1); // PT24H
        var hourly = Duration.ofHours(1); // PT1H
        var everyMinute = Duration.ofMinutes(1); // PT1M
        var everyTenSeconds = Duration.ofSeconds(10); // PT10S
        var everyMilli = Duration.ofMillis(1); // PT0.001S
        var everyNano = Duration.ofNanos(1); // PT0.000000001S

        //ChronoUnit
        var daily_c = Duration.of(1, ChronoUnit.DAYS);
        var hourly_c = Duration.of(1, ChronoUnit.HOURS);
        var everyMinute_c = Duration.of(1, ChronoUnit.MINUTES);
        var everyTenSeconds_c = Duration.of(10, ChronoUnit.SECONDS);
        var everyMilli_c = Duration.of(1, ChronoUnit.MILLIS);
        var everyNano_c = Duration.of(1, ChronoUnit.NANOS);
        var halfDay_c = Duration.of(1, ChronoUnit.HALF_DAYS);

        //find difference
        var one = LocalTime.of(5, 15);
        var two = LocalTime.of(6, 30);
        var date = LocalDate.of(2016, 1, 20);
        System.out.println(ChronoUnit.HOURS.between(one, two)); // 1
        System.out.println(ChronoUnit.MINUTES.between(one, two)); // 75
        //System.out.println(ChronoUnit.MINUTES.between(one, date)); // DateTimeException

        //Truncate
        LocalTime time = LocalTime.of(3,12,45);
        System.out.println(time); // 03:12:45
        LocalTime truncated = time.truncatedTo(ChronoUnit.MINUTES);
        System.out.println(truncated); // 03:12

        //Using duration
        var date_d = LocalDate.of(2023,8,5);
        var time_d = LocalTime.of(12,11);
        var dateTime = LocalDateTime.of(date_d, time_d);
        var duration_d = Duration.ofHours(6);
        System.out.println(dateTime.plus(duration_d)); // 2022–01–20T12:15
        System.out.println(time.plus(duration_d)); // 12:15

        //Instants
        var now = Instant.now();
        System.out.println(now);

        var date_i = LocalDate.of(2022, 5, 25);
        var time_i = LocalTime.of(11, 55, 00);
        var zone = ZoneId.of("US/Eastern");
        var zonedDateTime = ZonedDateTime.of(date_i, time_i, zone);
        var instant = zonedDateTime.toInstant(); // 2022–05–25T15:55:00Z
        System.out.println(zonedDateTime); // 2022–05–25T11:55–04:00[US/Eastern]
        System.out.println(instant); // 202–05–25T15:55:00Z
    }
}
