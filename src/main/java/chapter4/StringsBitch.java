package chapter4;

public class StringsBitch {
    public static void main(String[] args) {
        int three = 3;
        String four = "4";
        System.out.println(1 + 2 + three + four);


        var name = "animals";
        System.out.println(name.substring(3)); // mals
        System.out.println(name.substring(name.indexOf('m'))); // mals
        System.out.println(name.substring(3, 4)); // m
        System.out.println(name.substring(3, 7)); // mals
    }
}
