package chapter4;

public class StringBuilderClass {
    public static void main(String[] args) {
        StringBuilder alpha = new StringBuilder();
        for(char current = 'a'; current <= 'z'; current++)
            alpha.append(current);
        System.out.println(alpha);
    }
}
