package chapter4;

public class FormattingStrings {
    public static void main(String[] args) {
        var name = "Kate";
        var orderId = 5;
// All print: Hello Kate, order 5 is ready
        System.out.println("Hello " + name + ", order " + orderId + " is ready");
        System.out.printf("Hello %s, order %d is ready%n",
                name, orderId);
        System.out.printf("Hello %s, order %d is ready%n", name, orderId);


        var name1 = "James";
        var score = 90.25;
        var total = 100;
        System.out.printf("%s:%n Score: %f out of %d%n", name1, score, total);


        var pi = 3.14159265359;
        System.out.format("[%f]",pi); // [3.141593]
        System.out.format("[%12.8f]",pi); // [ 3.14159265]
        System.out.format("[%012f]",pi); // [00003.141593]
        System.out.format("[%12.2f]",pi); // [ 3.14]
        System.out.format("[%.3f]",pi); // [3.142]
    }
}
